<!-- Navigation -->
<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
	<#include "navbar-header.ftl">
	<#include "navbar-top.ftl">
	<#include "navbar-sidebar.ftl">
</nav>