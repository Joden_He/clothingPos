<!-- jQuery -->
<script src="${request.contextPath}/resources/sbAdmin/vendor/jquery/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="${request.contextPath}/resources/sbAdmin/vendor/bootstrap/js/bootstrap.min.js"></script>
<!-- Metis Menu Plugin JavaScript -->
<script src="${request.contextPath}/resources/sbAdmin/vendor/metisMenu/metisMenu.min.js"></script>
<!-- Morris Charts JavaScript -->
<script src="${request.contextPath}/resources/sbAdmin/vendor/raphael/raphael.min.js"></script>
<script src="${request.contextPath}/resources/sbAdmin/vendor/morrisjs/morris.min.js"></script>
<!-- Custom Theme JavaScript -->
<script src="${request.contextPath}/resources/sbAdmin/dist/js/sb-admin-2.js"></script>