package com.scau.jodenhe.cpos.dao;

import com.scau.jodenhe.cpos.entity.SaleRejectOrderItem;
import com.scau.jodenhe.cpos.plugins.mybatis.BaseMapper;

public interface SaleRejectOrderItemMapper extends BaseMapper<SaleRejectOrderItem> {
}