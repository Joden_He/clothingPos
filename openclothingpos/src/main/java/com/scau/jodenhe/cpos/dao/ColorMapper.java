package com.scau.jodenhe.cpos.dao;

import com.scau.jodenhe.cpos.entity.Color;
import com.scau.jodenhe.cpos.plugins.mybatis.BaseMapper;

public interface ColorMapper extends BaseMapper<Color> {
}