package com.scau.jodenhe.cpos.dao;

import com.scau.jodenhe.cpos.entity.Role;
import com.scau.jodenhe.cpos.plugins.mybatis.BaseMapper;

public interface RoleMapper extends BaseMapper<Role> {
}