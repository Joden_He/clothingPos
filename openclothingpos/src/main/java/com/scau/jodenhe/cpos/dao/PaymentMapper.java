package com.scau.jodenhe.cpos.dao;

import com.scau.jodenhe.cpos.entity.Payment;
import com.scau.jodenhe.cpos.plugins.mybatis.BaseMapper;

public interface PaymentMapper extends BaseMapper<Payment> {
}