package com.scau.jodenhe.cpos.dao;

import com.scau.jodenhe.cpos.entity.SaleRejectOrder;
import com.scau.jodenhe.cpos.plugins.mybatis.BaseMapper;

public interface SaleRejectOrderMapper extends BaseMapper<SaleRejectOrder> {
}