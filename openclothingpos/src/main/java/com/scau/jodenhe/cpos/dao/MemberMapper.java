package com.scau.jodenhe.cpos.dao;

import com.scau.jodenhe.cpos.entity.Member;
import com.scau.jodenhe.cpos.plugins.mybatis.BaseMapper;

public interface MemberMapper extends BaseMapper<Member> {
}