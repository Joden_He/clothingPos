package com.scau.jodenhe.cpos.dao;

import com.scau.jodenhe.cpos.entity.Category;
import com.scau.jodenhe.cpos.plugins.mybatis.BaseMapper;

public interface CategoryMapper extends BaseMapper<Category> {
}