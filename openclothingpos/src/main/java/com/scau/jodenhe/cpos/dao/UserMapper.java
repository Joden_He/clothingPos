package com.scau.jodenhe.cpos.dao;

import com.scau.jodenhe.cpos.entity.User;
import com.scau.jodenhe.cpos.plugins.mybatis.BaseMapper;

public interface UserMapper extends BaseMapper<User> {
}