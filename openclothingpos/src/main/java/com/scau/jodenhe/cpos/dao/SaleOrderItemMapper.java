package com.scau.jodenhe.cpos.dao;

import com.scau.jodenhe.cpos.entity.SaleOrderItem;
import com.scau.jodenhe.cpos.plugins.mybatis.BaseMapper;

public interface SaleOrderItemMapper extends BaseMapper<SaleOrderItem> {
}