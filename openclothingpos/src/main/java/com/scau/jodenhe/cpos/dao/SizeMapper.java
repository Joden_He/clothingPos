package com.scau.jodenhe.cpos.dao;

import com.scau.jodenhe.cpos.entity.Size;
import com.scau.jodenhe.cpos.plugins.mybatis.BaseMapper;

public interface SizeMapper extends BaseMapper<Size> {
}