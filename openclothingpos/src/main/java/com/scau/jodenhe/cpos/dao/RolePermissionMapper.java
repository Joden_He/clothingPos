package com.scau.jodenhe.cpos.dao;

import com.scau.jodenhe.cpos.entity.RolePermission;
import com.scau.jodenhe.cpos.plugins.mybatis.BaseMapper;

public interface RolePermissionMapper extends BaseMapper<RolePermission> {
}