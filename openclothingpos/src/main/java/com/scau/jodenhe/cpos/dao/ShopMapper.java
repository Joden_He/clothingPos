package com.scau.jodenhe.cpos.dao;

import com.scau.jodenhe.cpos.entity.Shop;
import com.scau.jodenhe.cpos.plugins.mybatis.BaseMapper;

public interface ShopMapper extends BaseMapper<Shop> {
}