package com.scau.jodenhe.cpos.dao;

import com.scau.jodenhe.cpos.entity.Inventory;
import com.scau.jodenhe.cpos.plugins.mybatis.BaseMapper;

public interface InventoryMapper extends BaseMapper<Inventory> {
}