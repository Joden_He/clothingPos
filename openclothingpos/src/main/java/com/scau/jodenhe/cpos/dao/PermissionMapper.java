package com.scau.jodenhe.cpos.dao;

import com.scau.jodenhe.cpos.entity.Permission;
import com.scau.jodenhe.cpos.plugins.mybatis.BaseMapper;

public interface PermissionMapper extends BaseMapper<Permission> {
}