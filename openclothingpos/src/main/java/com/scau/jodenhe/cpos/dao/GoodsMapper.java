package com.scau.jodenhe.cpos.dao;

import com.scau.jodenhe.cpos.entity.Goods;
import com.scau.jodenhe.cpos.plugins.mybatis.BaseMapper;

public interface GoodsMapper extends BaseMapper<Goods> {
}