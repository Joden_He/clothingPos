package com.scau.jodenhe.cpos.dao;

import com.scau.jodenhe.cpos.entity.SaleOrder;
import com.scau.jodenhe.cpos.plugins.mybatis.BaseMapper;

public interface SaleOrderMapper extends BaseMapper<SaleOrder> {
}