package com.scau.jodenhe.cpos.dao;

import com.scau.jodenhe.cpos.entity.UserRole;
import com.scau.jodenhe.cpos.plugins.mybatis.BaseMapper;

public interface UserRoleMapper extends BaseMapper<UserRole> {
}