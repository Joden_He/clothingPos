package com.scau.jodenhe.cpos.dao;

import com.scau.jodenhe.cpos.entity.Warehouse;
import com.scau.jodenhe.cpos.plugins.mybatis.BaseMapper;

public interface WarehouseMapper extends BaseMapper<Warehouse> {
}